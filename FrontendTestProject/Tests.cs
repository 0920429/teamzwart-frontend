using Frontend_UserStories.Data;
using Frontend_UserStories.ViewModels;
using System;
using Xunit;

namespace FrontendTestProject
{
    public class Tests
    {
        [Fact]
        public void GreenWhenHundred()
        {
            DefaultViewModel vw = new DefaultViewModel();
            string green = vw.GetCorrectStoriesColor(100);
            Assert.Equal("green", green);
        }

        [Fact]
        public void YellowHigherThanThirty()
        {
            DefaultViewModel vw = new DefaultViewModel();
            string yellow = vw.GetCorrectStoriesColor(35);
            Assert.Equal("yellow", yellow);
        }

        [Fact]
        public void RedLowerThanThirty()
        {
            DefaultViewModel vw = new DefaultViewModel();
            string red = vw.GetCorrectStoriesColor(0);
            Assert.Equal("red", red);
        }

        [Fact]
        public void LoadInvalidBoard()
        {
            TrelloDataStore.Instance.LoadBoardForUser("", "");
            Assert.Empty(TrelloDataStore.Instance.StoriesForUser);
        }

        [Fact]
        public void LoadBoardSuccess()
        {
            TrelloDataStore.Instance.LoadBoardForUser("8b8703af91b1b3dbb3fa12cef2bf43116fa8c6f2d6e410a82e88ade7e043fefd", "C9YBTVc7");
            Assert.NotEmpty(TrelloDataStore.Instance.StoriesForUser);
        }


        [Fact]
        public void LoadBoardInvalidToken()
        {
            TrelloDataStore.Instance.LoadBoardForUser("invalid", "C9YBTVc7");
            Assert.Empty(TrelloDataStore.Instance.StoriesForUser["invalid"].Item2);
        }
    }
}
