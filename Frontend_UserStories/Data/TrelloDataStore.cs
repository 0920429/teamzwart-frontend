﻿using DotVVM.Framework.Controls;
using Frontend_UserStories.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;


/// <summary>
/// Class that handles grabbing API and Trello data from their respective endpoints. 
/// © Rick Zegelaar
/// </summary>

namespace Frontend_UserStories.Data
{
    /// <summary>
    /// Class that represents a userstory for deserialization
    /// </summary>
    public class Story
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Role { get; set; }
        public string Mean { get; set; }
        public string Ends { get; set; }
        public int Points { get; set; }
        public List<Dictionary<string, string>> Suggestions { get; set; } = new List<Dictionary<string, string>>();
        public double Unique_Ratio { get; set; }
        public int Sentence_Length { get; set; }
        public int Words_Count { get; set; }

        public Story()
        {

        }

        public Story(int id, string text, string role, string mean, string ends, int points, List<Dictionary<string, string>> suggestions, double uniqueRatio, int sentenceLength, int wordsCount)
        {
            Id = id;
            Text = text;
            Mean = mean;
            Ends = ends;
            Points = points;
            Suggestions = suggestions;
            Unique_Ratio = uniqueRatio;
            Sentence_Length = sentenceLength;
            Words_Count = wordsCount;
        }
    }

    /// <summary>
    /// Class that represents a Trello board for deserialization purposes
    /// </summary>
    public class Board
    {
        private string boardId;
        private string boardName;

        public string BoardId { get => boardId; set => boardId = value; }
        public string BoardName { get => boardName; set => boardName = value; }

        public Board(string boardId, string boardName)
        {
            BoardId = boardId;
            BoardName = boardName;
        }

        public Board()
        {

        }
    }

    /// <summary>
    /// A singleton that represents the API. The choice was made to use a singleton since the frontend doesn't have a fixed state and initializing new instances of the API on each pageload would be costly.
    /// </summary>
    public sealed class TrelloDataStore
    {

        private static readonly TrelloDataStore instance = new TrelloDataStore();
        private Dictionary<string, (string, List<Story>)> userDictonary = new Dictionary<string, (string, List<Story>)>();
        private Dictionary<string, List<Board>> boardNamesForUser = new Dictionary<string, List<Board>>();

        private TrelloDataStore()
        {

        }

        /// <summary>
        /// Returns the singleton instance
        /// </summary>
        public static TrelloDataStore Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// Loads board for a given user
        /// </summary>
        /// <param name="token">The trello token associated with the user</param>
        /// <param name="BoardId">The boardID to load</param>
        public void LoadBoardForUser(string token, string BoardId)
        {
            // Check if we have a boardId
            if (BoardId == "None" || string.IsNullOrEmpty(token))
                return;

            // CHeck if BoardId is present
            if (string.IsNullOrEmpty(token))
                return;

            // Check if we already have the user token in our dictonary if we don't we add it and load the userstories
            // otherwise we replace the currently stored userstories with the ones we get
            if (!userDictonary.ContainsKey(token))
                userDictonary.Add(token, (BoardId, FetchStories(BoardId, token)));
            else
                userDictonary[token] = (BoardId, FetchStories(BoardId, token));
        }

        /// <summary>
        /// Helper method to get the userstory data from our backend
        /// </summary>
        /// <param name="BoardId">The Trello boardid to fetch stories for</param>
        /// <returns>A list of userstories</returns>
        private List<Story> FetchStories(string BoardId, string token)
        {
            WebClient wc = new WebClient();
            var json = wc.DownloadString("https://z0jyjfhij8.execute-api.eu-west-2.amazonaws.com/dev/trello?board=" + BoardId + "&token=" + token  + "&key=5ddd68c1f0218c6f900679cd59d06137");
            List <Story> stories = JsonConvert.DeserializeObject<List<Story>>(json);

            // Assign an ID starting at 1
            // TODO: See if we can do this prettier.
            for (int i = 0; i < stories.Count; i++)
            {
                stories[i].Id = 1 + i;
            }

            return stories;
        }

        /// <summary>
        /// Fetches all the boardnames associated with the user
        /// </summary>
        /// <param name="token">The trello token associated with the user</param>
        public void GetBoardNamesForUser(string token)
        { 
            // TODO: Should make this async, as it stand it locks up the page while loading (maybe also show a spinner or something)
            if (!boardNamesForUser.ContainsKey(token))
            {
                boardNamesForUser.Add(token, new List<Board>());
                WebClient wc = new WebClient();
                var user = wc.DownloadString("https://api.trello.com/1/members/me/" + "?key=5ddd68c1f0218c6f900679cd59d06137&token=" + token);

                JObject userObject = JObject.Parse(user);
                List<string> boardIds = userObject["idBoards"].ToObject<List<string>>();

                foreach (string boardId in boardIds)
                {
                    string json = wc.DownloadString("https://api.trello.com/1/boards/" + boardId + "?fields=name&key=5ddd68c1f0218c6f900679cd59d06137&token=" + token);
                    JObject jsonName = JObject.Parse(json);
                    string name = jsonName["name"].ToObject<string>();
                    boardNamesForUser[token].Add(new Board(boardId, name));
                }
            }
            return;           
        }

        public Dictionary<string, List<Board>> BoardsForUser
        {
            get
            {
                return boardNamesForUser;
            }
        }

        public Dictionary<string, (string, List<Story>)> StoriesForUser
        {
            get
            {
                return userDictonary;
            }
        }
    }
}
