﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotVVM.Framework.Hosting;
using DotVVM.Framework.ViewModel;
using Frontend_UserStories.Data;

namespace Frontend_UserStories.ViewModels
{
    public class MasterPageViewModel : DotvvmViewModelBase
    {
        public string UserName {get;set;}
        public List<Board> Boards { get; set; }

    public MasterPageViewModel()
        {
        }

        /// <summary>
        /// Called when the player click the logout button
        /// </summary>
        /// <returns></returns>
        public async Task Logout()
        {
            await Context.GetAuthentication().SignOutAsync("Cookies");
            Context.RedirectToRoute("Default");
        }

        /// <summary>
        /// Called when user selects a board from the dropdown
        /// </summary>
        /// <param name="boardId"></param>
        public void SelectBoard(string boardId)
        {
            TrelloDataStore.Instance.LoadBoardForUser(Context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "token").Value, boardId);
            Context.RedirectToRoute("Default");
        }
        
        public override Task Init()
        {
            // Try and load data when the user has been authenticated!
            if(Context.HttpContext.User.Identity.IsAuthenticated)
            {
                UserName = Context.GetAspNetCoreContext().User.Identity.Name;
                TrelloDataStore.Instance.GetBoardNamesForUser(Context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "token").Value);
                Boards = TrelloDataStore.Instance.BoardsForUser[Context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "token").Value];
            }

            return base.Init();
        }
    }
}
