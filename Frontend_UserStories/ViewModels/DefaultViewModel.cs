﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DotVVM.Framework.ViewModel;
using DotVVM.Framework.Hosting;
using DotVVM.Framework.Controls;
using Manatee.Trello;
using Manatee.Trello.ManateeJson;
using Manatee.Trello.WebApi;
using System.Net;
using Newtonsoft.Json;
using Frontend_UserStories.Data;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;

namespace Frontend_UserStories.ViewModels
{
    public class DefaultViewModel : MasterPageViewModel
    {
        public string Title { get; set; }
        public GridViewDataSet<Story> Stories {get; set;}
        public double CorrectStories { get; set; }
        public string CorrectStoriesColor { get; set; }

        private IEnumerable<Claim> claims { get; set; }
        public DefaultViewModel()
		{
            Title = "Overview";
        }

        private GridViewDataSetLoadedData<Story> GetData(IGridViewDataSetLoadOptions gridViewDataSetLoadOptions)
        {
            if (TrelloDataStore.Instance.StoriesForUser.ContainsKey(Context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "token").Value))
            {
                return TrelloDataStore.Instance.StoriesForUser[Context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "token").Value].Item2.AsQueryable().GetDataFromQueryable(gridViewDataSetLoadOptions);
            }
            else
                return null;
        }

        public async Task Login()
        {
            await Context.GetAuthentication().ChallengeAsync();
        }

        private double GetCorrectStories()
        {
            List<Story> stories = TrelloDataStore.Instance.StoriesForUser.ContainsKey(Context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "token").Value) ? TrelloDataStore.Instance.StoriesForUser[Context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "token").Value].Item2 : new List<Story>();

            int correctStories = 0;
            foreach (Story story in stories)
            {
                if (story.Points > 50)
                {
                    correctStories++;
                }
            }

            double result = stories.Count > 0 ? ((double) correctStories / (double) stories.Count) * 100 : 0;
            return Math.Round(result);
        }

        public string GetCorrectStoriesColor(double correctStories)
        {
            string result = "";
            if(correctStories == 100)
            {
                result = "green";
            } else if(correctStories < 100 && correctStories > 30)
            {
                result = "yellow";
            } else if(correctStories < 30)
            {
                result = "red";
            }

            return result;
        }

        public override Task Init()
        {
            // NOTE: You can also create the DataSet with factory.
            // Just call static Create with delegate and pagesize.
            //TrelloDataStore.Instance.LoadBoard("C9YBTVc7");
            if(Context.HttpContext.User.Identity.IsAuthenticated)
            {
                Stories = GridViewDataSet.Create(gridViewDataSetLoadDelegate: GetData, pageSize: 12);
                CorrectStories = GetCorrectStories();
                CorrectStoriesColor = GetCorrectStoriesColor(CorrectStories);
            }
            return base.Init();
        }
    }
}
