using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DotVVM.Framework.Controls;
using DotVVM.Framework.Hosting;
using DotVVM.Framework.ViewModel;
using Frontend_UserStories.Data;
using Newtonsoft.Json;

namespace Frontend_UserStories.ViewModels
{
    public class DetailStoryViewModel : MasterPageViewModel
    {
        [FromRoute("Id")]
        public int Id { get; set; }
        public string Text { get; set; }
        public string Role { get; set; }
        public string Mean { get; set; }
        public string Ends { get; set; }
        public int Points { get; set; }
        public List<Suggestion> Suggestions { get; set; }
        public List<Suggestion> Errors { get; set; }
        public double UniqueRatio { get; set; }
        public int SentenceLength { get; set; }
        public int WordsCount { get; set; }
        public List<Comment> Comments { get; set; } = new List<Comment>();
        public string NewCommentUserId { get; set; }
        public string NewCommentText { get; set; }

        public override Task Init()
        {
            Id = Convert.ToInt32(Context.Parameters["Id"]) - 1;
            var cards = TrelloDataStore.Instance.StoriesForUser[Context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == "token").Value].Item2;
            Text = cards[Id].Text;
            Role = cards[Id].Role;
            Mean = cards[Id].Mean;
            Ends = cards[Id].Ends;
            Points = cards[Id].Points;
            WordsCount = cards[Id].Words_Count;
            UniqueRatio = 100 - (Math.Round(cards[Id].Unique_Ratio, 2) * 100);
            Suggestions = GenerateSuggestions(cards[Id].Suggestions.SelectMany(v => v.Keys).ToList());
            Errors = GenerateWarnings();
            SentenceLength = cards[Id].Sentence_Length;
            return base.Init();
        }

        /// <summary>
        /// Generates the list of suggestions 
        /// </summary>
        /// <param name="suggestionList">List of identifiers</param>
        /// <returns>Suggestion List</returns>
        private List<Suggestion> GenerateSuggestions(List<string> suggestionList)
        {
            List<Suggestion> suggestions = new List<Suggestion>();
            foreach (string item in suggestionList)
            {
                switch (item)
                {
                    case "text_mean":
                        suggestions.Add(new Suggestion("Did you forget the task?", "When the user story doesn't clearly mention the feature you want to implement it could lead to ambiguity and the wrong thing might get implemented"));
                        break;
                    case "text_ends":
                        suggestions.Add(new Suggestion("Did you forgot the reason?", "When a user story lacks the reason why a user would want the feature, it might be possible that you'll be implementing something that isn't useful"));
                        break;
                    case "text_role":
                        suggestions.Add(new Suggestion("Did you forget the user's role?", "It's important to know what kind of user wants a user story since different roles require different features!"));
                        break;
                    default:
                        break;
                }
            }
            return suggestions;
        }

        /// <summary>
        /// Generate warnings
        /// </summary>
        /// <returns>List of warnings</returns>
        private List<Suggestion> GenerateWarnings()
        {
            List<Suggestion> warnings = new List<Suggestion>();
            if (UniqueRatio < 45)
                warnings.Add(new Suggestion("Not unique", "We detected that a large part of your userstory is a duplicate of an already existing one."));
            if (WordsCount < 20)
                warnings.Add(new Suggestion("Not descriptive", "When writing a userstory keep in mind that the team needs context to help understand an userstory"));

            return warnings;
        }

        public DetailStoryViewModel()
        {
            //TODO: add lambda support to get comments from DB
        }
        public void AddComment()
        {
            Comments.Add(new Comment(Context.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value, Context.HttpContext.User.Identity.Name, NewCommentText));
            //TODO:add lambda support to post comment to DB, need to implement a way to retrieve current Userid and send it with the post request
            NewCommentUserId = null;
            NewCommentText = null;
        }
    }

    public class Suggestion
    {
        public string Title { get; set; }
        public string Feedback { get; set; }

        public Suggestion(string title, string feedback)
        {
            Title = title;
            Feedback = feedback;
        }

        public Suggestion()
        {

        }
    }

    public class Comment
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }

        public Comment()
        {

        }
        public Comment(string userId, string userName, string text)
        {
            UserId = userId;
            UserName = userName;
            Text = text;
        }
    }
}
