// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// Modifications by Rick Zegelaar

using System;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;

namespace Frontend_UserStories.Trello
{
    public static class TrelloExtensions
    {
        public static AuthenticationBuilder AddTrello(this AuthenticationBuilder builder)
            => builder.AddTrello(TrelloDefaults.AuthenticationScheme, _ => { });

        public static AuthenticationBuilder AddTrello(this AuthenticationBuilder builder, Action<TrelloOptions> configureOptions)
            => builder.AddTrello(TrelloDefaults.AuthenticationScheme, configureOptions);

        public static AuthenticationBuilder AddTrello(this AuthenticationBuilder builder, string authenticationScheme, Action<TrelloOptions> configureOptions)
            => builder.AddTrello(authenticationScheme, TrelloDefaults.DisplayName, configureOptions);

        public static AuthenticationBuilder AddTrello(this AuthenticationBuilder builder, string authenticationScheme, string displayName, Action<TrelloOptions> configureOptions)
        {
            builder.Services.TryAddEnumerable(ServiceDescriptor.Singleton<IPostConfigureOptions<TrelloOptions>, TrelloPostConfigureOptions>());
            return builder.AddRemoteScheme<TrelloOptions, TrelloHandler>(authenticationScheme, displayName, configureOptions);
        }
    }
}
