// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
// Modifications by Rick Zegelaar

using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;

namespace Frontend_UserStories.Trello
{
    /// <summary>
    /// Contains information about the login session as well as the user <see cref="System.Security.Claims.ClaimsIdentity"/>.
    /// </summary>
    public class TrelloCreatingTicketContext : ResultContext<TrelloOptions>
    {
        /// <summary>
        /// Initializes a <see cref="TrelloCreatingTicketContext"/>
        /// </summary>
        /// <param name="context">The HTTP environment</param>
        /// <param name="scheme">The scheme data</param>
        /// <param name="options">The options for Trello</param>
        /// <param name="principal">The <see cref="ClaimsPrincipal"/>.</param>
        /// <param name="properties">The <see cref="AuthenticationProperties"/>.</param>
        /// <param name="userId">Trello user ID</param>
        /// <param name="userName">Trello screen name</param>
        /// <param name="accessToken">Trello access token</param>
        /// <param name="accessTokenSecret">Trello access token secret</param>
        /// <param name="user">User details</param>
        public TrelloCreatingTicketContext(
            HttpContext context,
            AuthenticationScheme scheme,
            TrelloOptions options,
            ClaimsPrincipal principal,
            AuthenticationProperties properties,
            string userId,
            string userName,
            string accessToken,
            string accessTokenSecret,
            JObject user)
            : base(context, scheme, options)
        {
            UserId = userId;
            UserName = userName;
            AccessToken = accessToken;
            AccessTokenSecret = accessTokenSecret;
            User = user ?? new JObject();
            Principal = principal;
            Properties = properties;
        }

        /// <summary>
        /// Gets the Trello user ID
        /// </summary>
        public string UserId { get; }

        /// <summary>
        /// Gets the Trello screen name
        /// </summary>
        public string UserName { get; }

        /// <summary>
        /// Gets the Trello access token
        /// </summary>
        public string AccessToken { get; }

        /// <summary>
        /// Gets the Trello access token secret
        /// </summary>
        public string AccessTokenSecret { get; }

        /// <summary>
        /// Gets the JSON-serialized user or an empty
        /// <see cref="JObject"/> if it is not available.
        /// </summary>
        public JObject User { get; }
    }
}
