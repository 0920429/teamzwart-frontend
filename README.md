# teamzwart-frontend
Project serverless frontend voor het uploaded van de user stories en tonen
To setup this project you need the following:

- Install DotVVM from https://www.dotvvm.com/download/dotvvm-for-visual-studio
- Open project
- Run from VS


// Frontend using AWS backend
http://teamzwartfrontend.azurewebsites.net/


// (NON AWS WITH TRELLO API IN C#, board selection)
Current URL running the non aws backend is http://frontenduserstories20180514025142.azurewebsites.net/
